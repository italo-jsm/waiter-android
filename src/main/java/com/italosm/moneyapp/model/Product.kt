package com.italosm.moneyapp.model


import java.math.BigDecimal


class Product(val code: String,
              val description: String,
              val saleCost: BigDecimal)