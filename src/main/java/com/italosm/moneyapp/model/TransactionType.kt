package com.italosm.moneyapp.model

enum class TransactionType {
    INCOME, EXPENSE
}