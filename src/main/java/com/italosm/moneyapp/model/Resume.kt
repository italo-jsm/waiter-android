package com.italosm.moneyapp.model

import java.math.BigDecimal

class Resume(private val transactions: List<Transaction>) {

    val incomeSum get() = sumByType(TransactionType.INCOME)

    val expenseSum get() = sumByType(TransactionType.EXPENSE)

    val total: BigDecimal get() = incomeSum.subtract(expenseSum)

    private fun sumByType(type: TransactionType) =
        transactions
            .filter { it.type == type }
            .sumByDouble { it.amount.toDouble()}.toBigDecimal()
}