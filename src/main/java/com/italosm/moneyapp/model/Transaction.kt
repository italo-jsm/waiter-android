package com.italosm.moneyapp.model

import java.math.BigDecimal
import java.time.LocalDate

class Transaction (val amount: BigDecimal,
                   val category: String = "Undefined",
                   val type: TransactionType,
                   val date: LocalDate = LocalDate.now())