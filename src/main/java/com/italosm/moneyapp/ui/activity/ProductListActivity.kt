package com.italosm.moneyapp.ui.activity

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.italosm.moneyapp.R
import com.italosm.moneyapp.model.Product
import com.italosm.moneyapp.retrofit.RetrofitInitializer
import com.italosm.moneyapp.ui.adapter.ProductListAdapter
import kotlinx.android.synthetic.main.product_list_activity.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.product_list_activity)
        requestProductsList()
    }

    private fun requestProductsList() {
        val list = RetrofitInitializer().productService().list()
        list.enqueue(object : Callback<List<Product>?> {
            override fun onResponse(call: Call<List<Product>?>?, response: Response<List<Product>?>?) {
                response?.body()?.let {
                    val products: List<Product> = it
                    configureList(products)
                }
            }

            override fun onFailure(call: Call<List<Product>?>?, t: Throwable?) {
                t?.message?.let { Log.e("onFailure error", it) }
            }
        })
    }

    private fun configureList(products: List<Product>) {
        val recyclerView = product_list_recycler_view
        recyclerView.adapter = ProductListAdapter(products, this)
        val layoutManager = StaggeredGridLayoutManager(
            1, StaggeredGridLayoutManager.VERTICAL)
        recyclerView.layoutManager = layoutManager
    }
}