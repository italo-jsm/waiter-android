package com.italosm.moneyapp.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.italosm.moneyapp.R
import com.italosm.moneyapp.model.Transaction
import com.italosm.moneyapp.model.TransactionType
import com.italosm.moneyapp.ui.ViewResume
import com.italosm.moneyapp.ui.adapter.TransactionListAdapter
import kotlinx.android.synthetic.main.activity_lista_transacoes.*
import java.math.BigDecimal
import java.time.LocalDate

class TransactionListActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_transacoes)

        val transactions: List<Transaction> = exampleTransactions()

        configureList(transactions)

        ViewResume(transactions, window.decorView).fillResumeFields()
    }

    private fun configureList(transactions: List<Transaction>) {
        lista_transacoes_listview.adapter = TransactionListAdapter(transactions, this)
    }

    private fun exampleTransactions(): List<Transaction> {
        return listOf(Transaction(
            type = TransactionType.EXPENSE,
            category = "almoço de final de semana",
            date = LocalDate.now(),
            amount = BigDecimal(20.5)
        ),
            Transaction(
            amount = BigDecimal(100.0),
            type = TransactionType.INCOME,
            category = "Economia"),
            Transaction(
            amount = BigDecimal(200.0),
            type = TransactionType.EXPENSE),
            Transaction(amount = BigDecimal(500.0),
            category = "Prêmio",
            type = TransactionType.INCOME))
    }
}