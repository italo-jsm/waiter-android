package com.italosm.moneyapp.ui

import android.view.View
import androidx.core.content.ContextCompat
import com.italosm.moneyapp.R
import com.italosm.moneyapp.model.Resume
import com.italosm.moneyapp.model.Transaction
import kotlinx.android.synthetic.main.resumo_card.view.*
import java.text.DecimalFormat
import java.util.*

class ViewResume (
    private val transactions: List<Transaction>,
    private val view: View){

    fun fillResumeFields(){
        val resume = Resume(transactions)
        with(view.resumo_card_total){
            text = DecimalFormat.getCurrencyInstance(Locale("pt", "br")).format(resume.total)
            if(resume.expenseSum > resume.incomeSum)setTextColor(ContextCompat.getColor(view.context, R.color.despesa))
            if(resume.expenseSum <= resume.incomeSum)setTextColor(ContextCompat.getColor(view.context, R.color.receita))
        }
        with(view.resumo_card_despesa){
            text = DecimalFormat.getCurrencyInstance(Locale("pt", "br")).format(resume.expenseSum)
            setTextColor(ContextCompat.getColor(view.context, R.color.despesa))
        }
        with(view.resumo_card_receita){
            text = DecimalFormat.getCurrencyInstance(Locale("pt", "br")).format(resume.incomeSum)
            setTextColor(ContextCompat.getColor(view.context, R.color.receita))
        }

    }


}