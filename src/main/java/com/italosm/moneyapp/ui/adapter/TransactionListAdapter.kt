package com.italosm.moneyapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.core.content.ContextCompat
import com.italosm.moneyapp.R
import com.italosm.moneyapp.model.Transaction
import com.italosm.moneyapp.model.TransactionType
import kotlinx.android.synthetic.main.transacao_item.view.*
import java.text.DecimalFormat
import java.time.format.DateTimeFormatter

class TransactionListAdapter (private val transactions: List<Transaction>,
                              private val context: Context) : BaseAdapter(){

    override fun getView(posicao: Int, view: View?, parent: ViewGroup?): View {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.transacao_item, parent, false)

        val transaction = transactions[posicao]

        addAmmount(transaction, view)
        addIcon(transaction, view)
        addCategory(view, transaction)
        addDate(view, transaction)

        return view
    }

    private fun addDate(view: View, transaction: Transaction) {
        view.transacao_data.text = transaction.date.format(DateTimeFormatter.ISO_LOCAL_DATE)
    }

    private fun addCategory(view: View, transaction: Transaction) {
        view.transacao_categoria.text = transaction.category
    }

    private fun addIcon(transaction: Transaction, view: View) {
        val icon = icon(transaction.type)
        view.transacao_icone
            .setBackgroundResource(icon)
    }

    private fun icon(type: TransactionType): Int {
        if (type == TransactionType.INCOME) {
            return R.drawable.icone_transacao_item_receita
        }
        return R.drawable.icone_transacao_item_despesa
    }

    private fun addAmmount(transaction: Transaction, view: View) {
        val cor: Int = colorBy(transaction.type)
        view.transacao_valor
            .setTextColor(cor)
        view.transacao_valor.text = DecimalFormat.getInstance().format(transaction.amount)
    }

    private fun colorBy(type: TransactionType): Int {
        if (type == TransactionType.INCOME) {
            return ContextCompat.getColor(context, R.color.receita)
        }
        return ContextCompat.getColor(context, R.color.despesa)
    }

    override fun getItem(position: Int): Transaction {
        return transactions[position]
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return transactions.size
    }
}