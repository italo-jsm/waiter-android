package com.italosm.moneyapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.italosm.moneyapp.R
import com.italosm.moneyapp.model.Product
import kotlinx.android.synthetic.main.product_item.view.*
import java.text.DecimalFormat
import java.util.*

class ProductListAdapter(private val products: List<Product>, private val context: Context) : RecyclerView.Adapter<ProductListAdapter.ViewHolder>()  {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.product_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return products.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val product = products[position]
        holder.bindView(product)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(product: Product) {
            val card = itemView.product_card
            card.product_code.text = product.code
            card.product_cost.text = DecimalFormat.getCurrencyInstance(Locale("pt", "br")).format(product.saleCost)
            card.product_description.text = product.description
        }
    }

}