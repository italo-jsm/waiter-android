package com.italosm.moneyapp.retrofit

import com.italosm.moneyapp.retrofit.service.ProductService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class RetrofitInitializer {

    private val retrofit = Retrofit.Builder()
        .baseUrl("http://192.168.0.21:8080/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun productService(): ProductService = retrofit.create(ProductService::class.java)
}