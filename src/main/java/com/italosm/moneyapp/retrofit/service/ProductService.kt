package com.italosm.moneyapp.retrofit.service

import com.italosm.moneyapp.model.Product
import retrofit2.Call
import retrofit2.http.GET

interface ProductService {
    @GET("products")
    fun list(): Call<List<Product>>
}